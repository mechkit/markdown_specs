# Markdown Specs

Converts markdown into specDOM.

## Class name

Anything inside "{" and "}" will be added as a class of the element.

Example:

    # Title {title}

    <h1 class="title"></h1>
